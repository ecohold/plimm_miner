
from qiskit import QuantumRegister, ClassicalRegister, QuantumCircuit
from numpy import pi

qreg_q = QuantumRegister(4, 'q')
creg_c = ClassicalRegister(4, 'c')
circuit = QuantumCircuit(qreg_q, creg_c)

# Adicione as portas quânticas e medidas conforme necessário

# Exemplo: Aplicando uma porta Hadamard no primeiro qubit
circuit.h(qreg_q[0])

# Exemplo: Aplicando uma porta CNOT entre o primeiro e o segundo qubit
circuit.cx(qreg_q[0], qreg_q[1])

# Exemplo: Medindo todos os qubits e armazenando os resultados nos registradores clássicos
circuit.measure(qreg_q, creg_c)

# Imprimindo o circuito gerado
print(circuit) 